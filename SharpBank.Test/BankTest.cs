﻿using NUnit.Framework;
using System;
namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account(Account.AccountType.CHECKING, Account.AccountInterest.ANUAL));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\r\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.CHECKING, Account.AccountInterest.ANUAL);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void CheckingAccountDailyInterestPaid_1()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.CHECKING, Account.AccountInterest.DAILY);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void CheckingAccountDailyInterestPaid_2()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.CHECKING, Account.AccountInterest.DAILY, DateTime.Parse(DateTime.Now.AddDays(-2).ToString()));
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0, DateTime.Parse(DateTime.Now.AddDays(-2).ToString()));

            Assert.AreEqual(0.2, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.SAVINGS, Account.AccountInterest.ANUAL);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithAccountActive()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.MAXI_SAVINGS, Account.AccountInterest.ANUAL);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0);

            Assert.AreEqual(3, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithOutAccountActive()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.MAXI_SAVINGS, Account.AccountInterest.ANUAL, DateTime.Parse(DateTime.Now.AddDays(-12).ToString()));
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0, DateTime.Parse(DateTime.Now.AddDays(-12).ToString()));

            Assert.AreEqual(150, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

    }
}
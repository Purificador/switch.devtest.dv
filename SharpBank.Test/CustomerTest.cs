﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account(Account.AccountType.CHECKING, Account.AccountInterest.ANUAL);
            Account savingsAccount = new Account(Account.AccountType.SAVINGS, Account.AccountInterest.ANUAL);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Account.AccountType.SAVINGS, Account.AccountInterest.ANUAL));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Account.AccountType.SAVINGS, Account.AccountInterest.ANUAL));
            oscar.OpenAccount(new Account(Account.AccountType.CHECKING, Account.AccountInterest.ANUAL));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Account.AccountType.SAVINGS, Account.AccountInterest.ANUAL));
            oscar.OpenAccount(new Account(Account.AccountType.CHECKING, Account.AccountInterest.ANUAL));
            oscar.OpenAccount(new Account(Account.AccountType.MAXI_SAVINGS, Account.AccountInterest.ANUAL));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void CustomerTransfer()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(Account.AccountType.MAXI_SAVINGS, Account.AccountInterest.ANUAL);
            Account savingsAccount = new Account(Account.AccountType.SAVINGS, Account.AccountInterest.ANUAL);

            Customer oscar = new Customer("Oscar").OpenAccount(checkingAccount).OpenAccount(savingsAccount);
            bank.AddCustomer(oscar);

            checkingAccount.Deposit(100);
            savingsAccount.Deposit(100);

            oscar.Transfer(100, savingsAccount, checkingAccount);

            Assert.AreEqual(0, savingsAccount.InterestEarned());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public enum AccountType
        {
            CHECKING = 0,
            SAVINGS = 1,
            MAXI_SAVINGS = 2
        }
        public enum AccountInterest
        {
            ANUAL = 0,
            DAILY = 1
        }

        private readonly AccountInterest InterestType;
        private readonly AccountType accountType;
        public List<Transaction> Transactions { get; set; }
        public DateTime AccountCreated { get; set; }

        public Account(AccountType accountType, AccountInterest accountInterest, DateTime? accountCreation = null)
        {
            this.accountType = accountType;
            this.InterestType = accountInterest;
            this.Transactions = new List<Transaction>();
            if (accountCreation == null)
            {
                AccountCreated = DateTime.Now;
            }
            else
            {
                try
                {
                    AccountCreated = DateTime.TryParse(accountCreation.ToString(), out DateTime retu) ? DateTime.Parse(accountCreation.ToString()) : throw new FormatException("Wrong Date Format");
                }
                catch (FormatException fex)
                {
                    throw fex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void Deposit(double amount, DateTime? transactionCreation = null)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                Transactions.Add(new Transaction(amount, transactionCreation));
            }
        }

        public void Withdraw(double amount, DateTime? transactionCreation = null)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                Transactions.Add(new Transaction(-amount, transactionCreation));
            }
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();
            if (this.InterestType == AccountInterest.DAILY)
            {
                int InterestDays = (DateTime.Now - AccountCreated).Days;

                return amount * 0.001 * InterestDays;
            }
            else
            {
                switch (accountType)
                {
                    case AccountType.SAVINGS:
                        if (amount <= 1000)
                            return amount * 0.001;
                        else
                            return 1 + (amount - 1000) * 0.002;
                    // case SUPER_SAVINGS:
                    //     if (amount <= 4000)
                    //         return 20;
                    case AccountType.MAXI_SAVINGS:
                        if (AccountActiveOnPassedDays())
                        {
                            return amount * 0.001;
                        }
                        else
                        {
                            return amount * 0.05;
                        }
                    //if (amount <= 1000)
                    //    return amount * 0.02;
                    //if (amount <= 2000)
                    //    return 20 + (amount - 1000) * 0.05;
                    //else
                    //    return 70 + (amount - 2000) * 0.1;
                    default:
                        return amount * 0.001;
                }
            }

        }

        public bool AccountActiveOnPassedDays()
        {
            bool ret = false;
            if (this.Transactions.Count > 0)
            {
                foreach (var t in this.Transactions)
                {
                    if (DateTime.Now.AddDays(-10) < t.TransactionDate && DateTime.Now > t.TransactionDate)
                    {
                        ret = true;
                        break;
                    }
                }
            }
            else
            {
                ret = false;
            }

            return ret;
        }

        public double SumTransactions()
        {
            return CheckIfTransactionsExist(true);
        }

        private double CheckIfTransactionsExist(bool checkAll)
        {
            double amount = 0.0;
            foreach (Transaction t in Transactions)
                amount += t.amount;
            return amount;
        }

        public AccountType GetAccountType()
        {
            return accountType;
        }

    }
}

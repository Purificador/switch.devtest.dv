﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private String Name { get; set; }
        private List<Account> Accounts { get; set; }

        public Customer(String name)
        {
            this.Name = name;
            this.Accounts = new List<Account>();
        }

        public Customer OpenAccount(Account account)
        {
            Accounts.Add(account);
            return this;
        }

        public string GetCustomerName()
        {
            return this.Name;
        }

        public int GetNumberOfAccounts()
        {
            return Accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in Accounts)
                total += a.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + Name + "\n";
            double total = 0.0;
            foreach (Account a in Accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.AccountType.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.AccountType.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.AccountType.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.Transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        public void Transfer(double amount, Account origin, Account destination, DateTime? transactionCreation = null)
        {
            if (amount < 0)
            {
                throw new ArgumentException("amount must be positive");
            }
            else
            {
                if (this.Accounts.Count > 1)
                {
                    origin.Withdraw(amount, transactionCreation);
                    destination.Deposit(amount, transactionCreation);
                }
                else
                {
                    throw new ArgumentException("Customer does not have 2 accounts");
                }
            }
        }

        private String ToDollars(double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
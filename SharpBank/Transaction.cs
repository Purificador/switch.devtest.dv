﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        public DateTime TransactionDate { get; set; }

        public Transaction(double amount, DateTime? transactionCreation = null)
        {
            this.amount = amount;
            if (transactionCreation == null)
            {
                this.TransactionDate = DateTime.Now;
            }
            else
            {
                try
                {
                    this.TransactionDate = DateTime.TryParse(transactionCreation.ToString(), out DateTime retu) ? DateTime.Parse(transactionCreation.ToString()) : throw new FormatException("Wrong Date Format");
                }
                catch (FormatException fex)
                {
                    throw fex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
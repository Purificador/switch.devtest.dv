﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> Customers { get; set; }

        public Bank()
        {
            Customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            Customers.Add(customer);
        }

        public String CustomerSummary()
        {
            String summary = "Customer Summary";
            foreach (Customer c in Customers)
            {
                summary += "\r\n - " + c.GetCustomerName() + " (" + Format(c.GetNumberOfAccounts(), "account") + ")";
            }
            return summary;
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private String Format(int number, String word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }

        public double TotalInterestPaid()
        {
            double total = 0;
            foreach (Customer c in Customers)
            {
                total += c.TotalInterestEarned();
            }
            return total;
        }

        public String GetFirstCustomer()
        {
            try
            {
                Customers = null;
                return Customers[0].GetCustomerName();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }
    }
}
